import espresso from "./images/coffee-espresso.png";
import latte from "./images/coffee-latte.png";
import coffeeToGo from "./images/coffee-to-go.png";
import "./App.css";

function App() {
  return (
    <div>
      <h2>Pick your coffee</h2>
      <div className="coffee-list">
        <img src={espresso} className="coffee" alt="espresso" />
        <img src={latte} className="coffee" alt="latte" />
        <img src={coffeeToGo} className="coffee" alt="to go" />
      </div>
    </div>
  );
}

export default App;
